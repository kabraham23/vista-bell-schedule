let currentTime, day, hours, minutes;
// const currentTime = new Date('September 1, 2023 09:11:00 PDT')
const output = document.getElementById("output")
/**
 * An object representing a class's start time
 * and end time. The start and end times are always 
 * represented as taking place on the current day. 
 * @typedef {Object} ClassPeriod
 * @property {Date} startTime - the class's start time
 * @property {Date} endTime - the class's end time
 * @property {number} length - the class's duration in minutes
 */


const MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000

setInterval(updateDom, 1000);

function updateDom() {
    currentTime = new Date()
    day = currentTime.getDay()
    hours = currentTime.getHours()
    minutes = currentTime.getMinutes()
    const currentBlockSchedule = getCurrentBlockSchedule(currentTime).map((block, i) => {
        let className = getClassName(i)
        block.className = className
        return block
    }).map(block => {
        const classPeriodObj = getClassPeriodObj(block)
        block.classPeriodObj = classPeriodObj
        return block
    })
    console.log(currentBlockSchedule)
    showTime()

    const currentBlock = currentBlockSchedule.find((block) => (currentTime > block.classPeriodObj.startTime && currentTime < block.classPeriodObj.endTime))

    if (!currentBlock) {
        output.innerText = `${"You don't have  a class right now!"}`
    } else { output.innerText = `${currentBlock.className}` }
}

// Defining showTime function
function showTime() {
    // Getting current time and date
    // let time = new Date();
    // using the global currentTime variable
    let hour = currentTime.getHours();
    let min = currentTime.getMinutes();
    let sec = currentTime.getSeconds();
    am_pm = "AM";

    // Setting time for 12 Hrs format
    if (hour >= 12) {
        if (hour > 12) hour -= 12;
        am_pm = "PM";
    } else if (hour == 0) {
        hr = 12;
        am_pm = "AM";
    }

    hour =
        hour < 10 ? "0" + hour : hour;
    min = min < 10 ? "0" + min : min;
    sec = sec < 10 ? "0" + sec : sec;

    // renaming this to avoid variable shadowing
    let displayTime =
        hour +
        ":" +
        min +
        ":" +
        sec +
        " " +
        am_pm;

    // Displaying the time
    document.getElementById(
        "clock"
    ).innerHTML = displayTime;
}

function getClassName(index) {
    const classSequenceArray = getDailyBlockSequence(day)
    return classSequenceArray[index]
}

/**
 * 
  @param {Object} options 
  @param {number} options.startHours 24h start time in local time
  @param {number} options.startMinutes 
  @param {number} options.length 
  @return {ClassPeriod} 
*/
function getClassPeriodObj({ startHours, startMinutes, length }) {
    const startTime = new Date()
    startTime.setHours(startHours)
    startTime.setMinutes(startMinutes)
    startTime.setSeconds(0)
    startTime.setMilliseconds(0)
    const endTime = new Date(startTime)
    endTime.setMinutes(endTime.getMinutes() + length)
    return { startTime, length, endTime }
}


/**
 * 
 * @param {Date} dateObj - today's date object
 * @returns {Array} - array of objects containing each block's 
 * startHours, startMinutes, and length.
 */
function getCurrentBlockSchedule(dateObj) {
    const blockScheduleObject = {
        mwrf: [
            {
                startHours: 8,
                startMinutes: 30,
                length: 40
            },
            {
                startHours: 9,
                startMinutes: 14,
                length: 82
            },
            {
                startHours: 10,
                startMinutes: 36,
                length: 12
            },
            {
                startHours: 10,
                startMinutes: 52,
                length: 82
            },
            {
                startHours: 12,
                startMinutes: 14,
                length: 30
            },
            {
                startHours: 12,
                startMinutes: 48,
                length: 82
            },
            {
                startHours: 14,
                startMinutes: 14,
                length: 82
            }
        ],
        t: [
            {
                startHours: 8,
                startMinutes: 30,
                length: 44
            },
            {
                startHours: 9,
                startMinutes: 18,
                length: 36
            },
            {
                startHours: 9,
                startMinutes: 58,
                length: 36
            },
            {
                startHours: 10,
                startMinutes: 34,
                length: 12
            },
            {
                startHours: 10,
                startMinutes: 50,
                length: 36
            },
            {
                startHours: 11,
                startMinutes: 30,
                length: 36
            },
            {
                startHours: 12,
                startMinutes: 10,
                length: 36
            },
            {
                startHours: 12,
                startMinutes: 46,
                length: 30
            },
            {
                startHours: 13,
                startMinutes: 20,
                length: 36
            },
            {
                startHours: 14,
                startMinutes: 0,
                length: 36
            }
        ]
    }
    const day = dateObj.getDay()
    return day == 2 ? blockScheduleObject.t : blockScheduleObject.mwrf
}



/**
 * 
 * @param {Date} day - today's date
 * @returns {Array} - class names in today's sequence
 */
function getDailyBlockSequence(day) {
    const dayIndex = day - 1
    const dailyBlockSequence = [
        ["Homeroom",
            "Period 1",
            "Nutrition",
            "Period 3",
            "Lunch",
            "Period 5",
            "Period 7"],
        ["Period 2",
            "Period 3",
            "Period 4",
            "Nutrition",
            "Period 5",
            "Period 6",
            "Period 7",
            "Lunch",
            "Period 8",
            "Period 1"
        ],
        ["Homeroom",
            "Period 4",
            "Nutrition",
            "Period 6",
            "Lunch",
            "Period 8",
            "Period 2"],
        ["Homeroom",
            "Period 5",
            "Nutrition",
            "Period 7",
            "Lunch",
            "Period 1",
            "Period 3"],
        ["Homeroom",
            "Period 6",
            "Nutrition",
            "Period 8",
            "Lunch",
            "Period 2",
            "Period 4"]
    ]
    return dailyBlockSequence[dayIndex - 1]
}

/**
 * 
 * @param {Object}  options             
 * @param {Date}    options.date        the class's starting time
 * @param {string}  options.unit        the unit of time to be added
 * @param {number}  options.duration    the length of the class
 * @return {Date}   Date object representing the end time of class
 */
